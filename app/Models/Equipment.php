<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Equipment extends Model
{
    protected $table = 'equipments';
    use HasFactory;


    public static function getTotal(): array
    {
        $sql = <<<SQL
                SELECT
                       e.name ,
                       sum(quantity) AS 'quantity'
                FROM equipment_statiON es
                JOIN equipments e ON e.id = es.equipment_id
                GROUP BY equipment_id
            SQL;

        return DB::SELECT($sql);
    }

    public static function getStationEquipmentDashboardData($start_date, $end_date, $dates = []): array
    {
        $sql = self::getEquipmentDataForOrderDates(); // this query gets data for order dates

        $sql2 = self::getEquipmentAvailabilityQuery(); // refactored version gets data for all days

        // formats the result to group all by date
        return self::formatData(DB::select($sql2, [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'max_date' => '2021-10-01' // remove if $sql is run instead
        ]));

    }

    private static function formatData(array $data, $dates = []): array
    {
        $statistics = [];
        foreach ($data as $item) {
            if(!array_key_exists($item->start_date, $statistics)) {
                $date = $statistics[$item->start_date] = [];
            }

            if (!array_key_exists($item->station,$statistics[$item->start_date])) {
                $statistics[$item->start_date][$item->station] = [];
            }

            if(!array_key_exists('equipments',$statistics[$item->start_date][$item->station])) {
                $statistics[$item->start_date][$item->station]['equipments'] = [];
            }
            $statistics[$item->start_date][$item->station]['equipments'][$item->equipment] = [
                'ordered' => $item->quantity_ordered,
                'available' => $item->available,
                'receiving' => $item->receiving_quantity ?? 0,
            ];


        }

        return $statistics;
    }

    /**
     * @return string
     */
    protected static function getEquipmentDataForOrderDates(): string
    {
        return <<<SQL
                        SELECT
                            e.name AS 'equipment',
                            s.name AS 'station',
                            o.start_date,
                            o.end_date,
                            ifnull(sum(oi.quantity), 0) AS quantity_ordered,
                            ifnull(( SELECT
                                    (
                                        SELECT sum(quantity)
                                        FROM
                                             equipment_station es
                                        WHERE es.equipment_id = e.id AND es.station_id = s.id)
                                    -
                                    ifnull((
                                        SELECT sum(oi2.quantity)
                                        FROM
                                            order_items oi2
                                        JOIN
                                                orders o2 ON o2.id = oi2.order_id
                                        WHERE
                                            o2.booking_station_id = s.id AND e.id = oi2.equipment_id AND o2.start_date <= o.start_date
                                    ), 0)
                            ), 0) AS 'available',
                            ifnull((
                                SELECT sum(oi2.quantity)
                                FROM
                                    order_items oi2
                                JOIN
                                    orders o2 ON o2.id = oi2.order_id
                                WHERE
                                    o2.returning_station_id = s.id AND e.id = oi2.equipment_id AND o2.end_date = o.start_date
                            ), 0) AS 'receiving_quantity'
                        FROM
                            equipments e
                        JOIN
                            equipment_station es ON e.id = es.equipment_id
                        JOIN
                            stations s ON s.id = es.station_id
                        JOIN
                            orders o ON o.booking_station_id = s.id
                        LEFT JOIN
                            order_items oi ON oi.order_id = o.id  AND oi.equipment_id = e.id
                        WHERE
                            o.start_date BETWEEN :start_date AND :end_date
                        GROUP BY
                            e.id, s.id, o.start_date
                        ORDER BY
                            o.start_date, e.name
                SQL;
    }

    /**
     * @return string
     */
    protected static function getEquipmentAvailabilityQuery(): string
    {
        return <<<SQL

                select
                days.selected_date AS 'start_date',
                s.name AS 'station',
                e.name as "equipment",
                ifnull((
                    select sum(oi.quantity) from orders o
                    join order_items oi on oi.order_id = o.id
                    where oi.equipment_id = e.id and o.start_date = days.selected_date and o.booking_station_id = s.id
                ), 0) as 'quantity_ordered',
                ( select
                        (select sum(quantity) from equipment_station es where es.equipment_id = e.id and es.station_id = s.id)
                        -
                        ifnull((
                            select sum(oi2.quantity) from
                            order_items oi2
                            join orders o2 on o2.id = oi2.order_id
                            where o2.booking_station_id = s.id and e.id = oi2.equipment_id and o2.start_date <= days.selected_date
                        ), 0)
                ) as 'available',
                ifnull((
                select sum(oi2.quantity) from
                order_items oi2
                join orders o2 on o2.id = oi2.order_id
                where o2.returning_station_id = s.id and e.id = oi2.equipment_id and o2.end_date = days.selected_date
                ), 0) as 'receiving_quantity'
                from
                (select * from
                            (select adddate(:max_date,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) selected_date from
                             (select 0 t0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                             (select 0 t1 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                             (select 0 t2 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                             (select 0 t3 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                             (select 0 t4 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
                 where selected_date between :start_date and :end_date) days
                 join equipments e
                 join stations s ORDER BY e.name, start_date

            SQL;
    }
}
