<?php

namespace App\Http\Controllers;

use App\Models\Equipment;
use App\Models\Station;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EquipmentController extends Controller
{
    /**
     * @throws Exception
     */
    public function index()
    {
        $totalEquipments =  Equipment::getTotal();
        $dates = $this->getDates();

        $equipmentStatistics = Equipment::getStationEquipmentDashboardData($dates['start'], $dates['end']);
        $headings = Station::all()->pluck('name');
        $dates = collect(CarbonPeriod::create($dates['start'], $dates['end'])->map(function ($date){
            return $date->format('Y-m-d');
        }))->toArray();

        if (request()->ajax() || request()->expectsJson()) {
            return [
                'status' => 'success',
                'data' => [
                    'statistics' => $equipmentStatistics,
                    'total_equipments' => $totalEquipments
                ],
            ];
        }

        return view('equipment.dashboard', compact('equipmentStatistics', 'headings', 'dates'));


    }

    /**
     * @throws Exception
     */
    protected function getDates(): array
    {
        $now = now();
        if (request()->has('start_date')) {
            $startDate = Carbon::parse(request('start_date'));
//            if($startDate->lt($now)) {
//                throw new Exception('Only dates in the future are allowed');
//            }
        } else {
            $startDate = $now->addDays(1);
            $tempDate = $startDate;
        }
        $tempDate = clone $startDate;
        if (request()->has('end_date')) {
            $endDate = Carbon::parse(request('end_date'));
        } else {
            $endDate = $tempDate->addDays(30);
        }

        if ($endDate->lt($startDate)) {
            throw new Exception('End date can not be lesser than start date');
        }

        return [
            "start" => $startDate->format('Y-m-d'),
            "end" => $endDate->format('Y-m-d')
        ];
    }


}
