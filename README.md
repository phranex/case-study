
## Road Surfer Case Study

Equipment Dashboard

- clone this repo
- create an .env from .env.example
- configure your DB settings in the .env (Very important!!)
- run the `install.sh` at the root directory (this file was created in a windows environment. Be careful of line spacing)
- The shell script will install all dependencies, migrate, seed the db and prepare the unit test environment
- The application will start at `localhost:8000`
- The equipment dashboard can be found at `localhost:8000/api/equipments?start_date=<start>&end_date=<end_date>`
- Adjust the date filters to generate different data

Here's an example of what you should see


![frontend](public/roadsurfer.png)

- orange represents total orders on that equipment
- blue represents total available equipment
- grey represents equipments to be received from a closing order
