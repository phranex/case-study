<?php

namespace Tests\Feature;

use App\Models\Camper;
use App\Models\Equipment;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Station;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EquipmentControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testThatAnHTMLTableIsReturned()
    {
        $response = $this->get('/api/equipments');
        $response->assertSee('equipments already book');
        $response->assertSee('table');
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertStatus(200);
    }

    public function testThatAllStationNamesArePresentInTheTable()
    {
        (new DatabaseSeeder())->run();
        $response = $this->get('/api/equipments');
        foreach (Station::all() as $station) {
            $response->assertSee(ucwords($station->name));
        }
        $response->assertStatus(200);
    }

    public function testThatAPIIsSupported()
    {
        (new DatabaseSeeder())->run();
        $response = $this->get('/api/equipments',[
            'Accept' => 'application/json'
        ]);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertStatus(200);
    }

    public function testThatEquipmentStatisticsAreValid()
    {
        $orderQuantity = 3;
        $initialQuantity = 10;
        $this->prepareTestData($initialQuantity, $orderQuantity);
        $response = $this->get('/api/equipments?start_date=2021-11-01&end_date=2021-12-31', [
            'Accept' => 'application/json'
        ]);
        $data = json_decode($response->getContent());
        $response->assertJsonStructure([
            'status',
            'data' => [
                'statistics' => []
            ]
        ]);

        $statistics = (array) $data->data->statistics;
        $this->assertArrayHasKey('2021-11-10', $statistics);
        $this->assertArrayHasKey('test_station', (array) $statistics['2021-11-10']);
        $this->assertArrayHasKey('equipments', (array) $statistics['2021-11-10']->{'test_station'});
        $this->assertArrayHasKey('test_equipment',
            (array) $statistics['2021-11-10']->{'test_station'}->equipments);
        $equipment = $statistics['2021-11-10']->{'test_station'}->equipments->{'test_equipment'};

        $this->assertEquals($orderQuantity, $equipment->ordered);
        $available = $initialQuantity - $orderQuantity;
        $this->assertEquals($available, $equipment->available);
    }

    /**
     * @param int $initialQuantity
     * @param int $orderQuantity
     * @return Collection|Model
     */
    protected function prepareTestData(int $initialQuantity, int $orderQuantity)
    {
        $bookingStation = Station::factory()->create([
            'name' => 'test_station'
        ]);
        $receivingStation = Station::factory()->create([
            'name' => 'receiving_station'
        ]);
        $equipment = Equipment::factory()->create([
            'name' => 'test_equipment'
        ]);
        $camper = Camper::factory()->create([
            'model_name' => 'TEST-123'
        ]);
        $bookingStation->equipments()->attach($equipment->id, [
            'quantity' => $initialQuantity,
        ]);

        $bookingStation->campers()->attach($camper->id, [
            'quantity' => $initialQuantity
        ]);

        $order = Order::factory()->create([
            'booking_station_id' => $bookingStation->id,
            'returning_station_id' => $receivingStation->id,
            'camper_id' => $camper->id,
            'start_date' => '2021-11-10',
            'end_date' => '2021-11-12', //
        ]);

        OrderItem::factory()->create([
            'equipment_id' => $equipment->id,
            'order_id' => $order->id,
            'quantity' => $orderQuantity,
        ]);
        return $equipment;
    }
}
