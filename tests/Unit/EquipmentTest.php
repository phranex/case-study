<?php

namespace Tests\Unit;

use App\Models\Equipment;
use App\Models\Station;
use Carbon\Carbon;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class EquipmentTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetTotal()
    {
        Equipment::factory()->create([
           'name' => 'test'
        ]);
        $this->assertSame(1, Equipment::all()->count());
    }

    public function testGetStationEquipmentDashboardData()
    {
        (new DatabaseSeeder())->run();
        $start = '2021-12-01';
        $end = '2021-12-21';

        $data = Equipment::getStationEquipmentDashboardData($start, $end);
        $keys = array_keys($data);
        $this->assertIsArray($data);
        $stationLayer = array_shift($data);

        $this->assertIsArray($stationLayer);
        $this->assertTrue(Carbon::parse($keys[0])->isValid());

        $station  = array_keys($stationLayer);
        $this->assertIsArray($stationLayer);
        $equipmentLayer = array_shift($stationLayer);
        $this->assertTrue(in_array($station[0], Station::all()->pluck('name')->toArray()));
        $this->assertArrayHasKey('equipments', $equipmentLayer);
        $this->assertIsArray($equipmentLayer['equipments']);
    }
}
