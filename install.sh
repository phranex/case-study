cp .env.example .env
composer install
php artisan migrate && php artisan migrate --database=test
php artisan db:seed
php artisan serve
