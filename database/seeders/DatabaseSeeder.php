<?php

namespace Database\Seeders;

use App\Models\Camper;
use App\Models\Equipment;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Station;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    // increase this as you like :)
    private const MAX_NUM_OF_ORDERS = 50;
    private const FUTURE_ORDER_START_DATE = '2021-12-01';
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Station::truncate();
        Equipment::truncate();
        Order::truncate();
        OrderItem::truncate();
        Station::truncate();
        DB::statement('DELETE FROM equipment_station');
        DB::statement('DELETE FROM camper_station');
        Schema::enableForeignKeyConstraints();

        // create stations
        Station::factory()->createMany([
            ['name' => 'berlin'],
            ['name' => 'dortmund'],
            ['name' => 'hamburg'],
            ['name' => 'cologne'],
            ['name' => 'dusseldorf'],
            ['name' => 'munich'],
            ['name' => 'frankfurt'],
        ]);

        // create equipments
        Equipment::factory()->createMany([
           ['name' => 'towels'],
           ['name' => 'toilet'],
           ['name' => 'fridge'],
           ['name' => 'bedsheet'],
           ['name' => 'pillow'],
           ['name' => 'torch'],
           ['name' => 'table'],
           ['name' => 'chair'],
           ['name' => 'mat'],
        ]);

        // create campers
        $campers = [];
        $numOfCampers = rand(10, 20);
        $alphabets = range('A', 'Z');
        for ($i = 0; $i < $numOfCampers; $i++) {
            $campers[] = [
                'model_name' => $alphabets[rand(0,24)] . $alphabets[rand(0,24)] . '-' . time(),
            ];
        }
        Camper::factory()->createMany($campers);

        // allot equipments and campers to stations
        foreach (Station::all() as $station) {
            foreach (Equipment::all() as $equipment) {
                $station->equipments()->attach($equipment->id,[
                    'quantity' => rand(10, 50),
                ]);
            }
            foreach (Camper::all() as $camper) {
                $station->campers()->attach($camper->id,[
                    'quantity' => rand(10, 50),
                ]);
            }
        }

        for ($i = 0; $i < self::MAX_NUM_OF_ORDERS; $i++) {
            $booking_station = Station::inRandomOrder()->first();
            $returning_station = Station::where('id', '!=', $booking_station->id)->inRandomOrder()->first();
            $camper = $booking_station->campers()->inRandomOrder()->first();

            // create an order
            $startDate = Carbon::parse(self::FUTURE_ORDER_START_DATE)->addDays(rand(1,31));
            $endDate = clone $startDate;
            $order = Order::factory()->create([
                'booking_station_id' => $booking_station->id,
                'returning_station_id' => $returning_station->id,
                'camper_id' => $camper->id,
                'start_date' => $startDate,
                'end_date' => $endDate->addDays(rand(5,31)), //
            ]);

            // randomly select the number of equipments this order should have
            $equipments = Equipment::inRandomOrder()->take(rand(1,9))->get();

            foreach ($equipments as $equipment) {
                $availableQuantityForEquipment = $order->booking
                    ->equipments()
                    ->wherePivot('equipment_id', $equipment->id)
                    ->first()
                    ->pivot
                    ->quantity;
                $numOfQuantityOrderedAlready = $order->booking->orderItems()->where('equipment_id', $equipment->id)->pluck('quantity')->sum();
                if(!$numOfQuantityOrderedAlready) $numOfQuantityOrderedAlready = 0;
                $maxQuantityAllowed = $availableQuantityForEquipment - $numOfQuantityOrderedAlready;
                if ($maxQuantityAllowed > 0) {
                    OrderItem::factory()->create([
                        'equipment_id' => $equipment->id,
                        'order_id' => $order->id,
                        'quantity' => rand(1, $maxQuantityAllowed),
                    ]);
                }

            }
        }



    }
}
