<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_station_id');
            $table->unsignedBigInteger('returning_station_id');
            $table->unsignedBigInteger('camper_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();

            $table->foreign('booking_station_id')->references('id')->on('stations');
            $table->foreign('returning_station_id')->references('id')->on('stations');
            $table->foreign('camper_id')->references('id')->on('campers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
