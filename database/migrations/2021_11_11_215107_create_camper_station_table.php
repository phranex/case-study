<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCamperStationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camper_station', function (Blueprint $table) {
            $table->unsignedBigInteger('camper_id');
            $table->unsignedBigInteger('station_id');
            $table->integer('quantity');
            $table->timestamps();
            $table->primary(['camper_id', 'station_id']);
            $table->foreign('camper_id')->references('id')->on('campers');
            $table->foreign('station_id')->references('id')->on('stations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camper_station');
    }
}
